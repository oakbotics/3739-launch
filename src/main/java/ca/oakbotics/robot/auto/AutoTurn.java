/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.auto;

import com.kauailabs.navx.frc.AHRS;

import ca.oakbotics.robot.subsystems.DriveBase;
import edu.wpi.first.wpilibj.PIDController;
import edu.wpi.first.wpilibj.PIDOutput;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class AutoTurn extends Command {
  public final PIDController gyroPID;
  private final double Kp = 0.0223, Ki = 0.000001, Kd = 0.1265;
  private double rotate;
  private AHRS ahrs;
  private double setPoint = 0;

  public AutoTurn(double setPoint) {
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
    requires(DriveBase.getInstance());

    ahrs = DriveBase.getInstance().getNavxBoard();

    this.setPoint = setPoint;
    gyroPID = new PIDController(Kp, Ki, Kd, ahrs, new PIDOutput(){
      @Override
      public void pidWrite (double output){
        System.out.println(output);
        rotate = output;
      }
    });

    gyroPID.setName("Drive", "Gyro PID");
    SmartDashboard.putData(gyroPID);
    
    gyroPID.setOutputRange(-0.75, 0.75);
    gyroPID.setInputRange(-180.0, 180.0);
    gyroPID.setContinuous(true);
    gyroPID.setAbsoluteTolerance(1.5);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
      gyroPID.reset();
      this.setPoint += ahrs.getAngle();
      this.setPoint = ((this.setPoint+180)%360)-180;
      System.out.println(this.setPoint);
      gyroPID.setSetpoint(this.setPoint);
      gyroPID.enable();
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    System.out.println("Output: " + rotate);
    System.out.println("Error: " + gyroPID.getError());
    
    double minR = 0.15;
    if(Math.abs(rotate) < minR){
        rotate = Math.signum(rotate)*minR;
    }
    DriveBase.getInstance().arcadeDrive(0, rotate);
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return gyroPID.onTarget();
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
    System.out.println(gyroPID.getError());
    gyroPID.disable();
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
    gyroPID.disable();
  }
}
