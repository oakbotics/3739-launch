package ca.oakbotics.robot;

import ca.oakbotics.robot.commands.CycleElevatorHeight;
import ca.oakbotics.robot.commands.CycleRollerBarAngle;
import ca.oakbotics.robot.commands.ResetArmAngle;
import ca.oakbotics.robot.commands.ResetElevatorHeight;
import ca.oakbotics.robot.commands.SetElevatorHeightOperator;
import ca.oakbotics.robot.commands.SetRollerBarAngleOperator;
import ca.oakbotics.robot.commands.ToggleForebar;
import ca.oakbotics.robot.commands.ToggleHatchMechArm;
import ca.oakbotics.robot.commands.ToggleHatchMechGrabber;
import ca.oakbotics.robot.commands.TogglePole;
import edu.wpi.first.wpilibj.GenericHID.Hand;
import edu.wpi.first.wpilibj.XboxController;
import edu.wpi.first.wpilibj.buttons.JoystickButton;

public class OI {

    private XboxController driveController;
    private XboxController operatorController;

    private static OI oi;
    private final static double kDRIVE_DEADBAND = 0.15;

    private JoystickButton driverA;
    private JoystickButton operA, operB, operX, operY, operRB, operLB, operLStick, operRStick, operStart, operSelect;

    private final static double kELEVATOR_MAX_ACCELERATION = 0.25;
    private final static double kARM_ANGLE_MAX_ACCELERATION = 0.5;

    private OI() {
        driveController = new XboxController(0);
        operatorController = new XboxController(1);

        driverA = new JoystickButton(driveController, 1);

        driverA.whenPressed(new TogglePole());

        operA = new JoystickButton(operatorController, 1);
        operA.toggleWhenPressed(new ToggleForebar());
        operB = new JoystickButton(operatorController, 2);
        operB.toggleWhenPressed(new ToggleHatchMechArm());
        operX = new JoystickButton(operatorController, 3);
        operY = new JoystickButton(operatorController, 4);
        operY.whenPressed(new ToggleHatchMechGrabber());

        operLB = new JoystickButton(operatorController, 5);
        operRB = new JoystickButton(operatorController, 6);
        operLB.whenPressed(new CycleRollerBarAngle());
        operRB.whenPressed(new CycleElevatorHeight());

        operSelect = new JoystickButton(operatorController, 7);
        operStart = new JoystickButton(operatorController, 8);
        operSelect.whenPressed(new ResetArmAngle(0));
        operStart.whenPressed(new ResetElevatorHeight(0));
        
        operLStick = new JoystickButton(operatorController, 9);
        operLStick.whenPressed(new SetRollerBarAngleOperator());

        operRStick = new JoystickButton(operatorController, 10);
        operRStick.toggleWhenPressed(new SetElevatorHeightOperator());

    }

    public static OI getInstance() {
        if (oi == null) {
            oi = new OI();
        }
        return oi;
    }

    public XboxController getDriveController() {
        return driveController;
    }

    public XboxController getOperatorController() {
        return operatorController;
    }

    public boolean safetyOverride() {
        return driveController.getStartButton();
    }

    public double getDriveSpeed() {
        double base = -driveController.getY(Hand.kLeft);
        double throt = driveController.getTriggerAxis(Hand.kRight);
        base = (Math.abs(base) >= kDRIVE_DEADBAND) ? base : 0;
        throt = (Math.abs(throt) >= kDRIVE_DEADBAND) ? throt : 0;
        //taha and joud changed this VV
        //return base - (0.5 * throt);
        return base * 0.5 + (base * (0.5 + throt));
    }

    public double getTurnSpeed() {
        
        double throt = driveController.getTriggerAxis(Hand.kRight);
        double base = driveController.getX(Hand.kRight);
        base = (Math.abs(base) >= kDRIVE_DEADBAND) ? base : 0;
        throt = (Math.abs(throt) >= kDRIVE_DEADBAND) ? throt : 0;
        //taha and joud changed this VV
        return base * 0.4 + (base * (0.6 * throt));
    }

    public double getIntakeSpeed() {
        return operatorController.getTriggerAxis(Hand.kRight) - operatorController.getTriggerAxis(Hand.kLeft);
    }

    public double getElevatorHeight() {
        double operVal = -operatorController.getY(Hand.kRight);
        double currentHeight = Robot.ELEVATOR.getHeightSetpoint();
        if (Math.abs(operVal) > 0.15) {
            operVal = operVal * operVal * Math.signum(operVal);
            if (safetyOverride() || (!((operVal < 0 && currentHeight == 0) || (operVal > 0 && currentHeight > 174)))) {
                currentHeight += kELEVATOR_MAX_ACCELERATION * Robot.getLastLoopTime() * operVal * Constants.kELEVATOR_MAX_HEIGHT;
            }
            if (!safetyOverride() && currentHeight >= 174)
                currentHeight = 174;
            else if (!safetyOverride() && currentHeight <= 0)
                currentHeight = 0;
        }
        return currentHeight;
    }

    public double getRollerVelocity() {
        return operatorController.getY(Hand.kLeft);
    }

    public double getRollerBarAngle() {
        double operVal = - operatorController.getY(Hand.kLeft);
        double currentAngle =  Robot.ROLLER_ARMS.getDesiredAngle(); 
        if (Math.abs(operVal) > 0.1) {
            if (safetyOverride() || (!((operVal < 0 && currentAngle == 0) || (operVal > 0 && currentAngle > Constants.kARM_MAX_ANGLE)))) {
                currentAngle += kARM_ANGLE_MAX_ACCELERATION * Robot.getLastLoopTime() * operVal * Constants.kARM_MAX_ANGLE;
            }
            if (!safetyOverride() && currentAngle >= Constants.kARM_MAX_ANGLE) {
                currentAngle = Constants.kARM_MAX_ANGLE;
            }
            else if (!safetyOverride() && currentAngle <= 0) {
                currentAngle = 0;
            }
        }
        return currentAngle;
    }
}