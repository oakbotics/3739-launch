/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.subsystems.Claw;
import ca.oakbotics.robot.subsystems.RollerBar;
import edu.wpi.first.wpilibj.command.Command;

public class IntakeCargo extends Command {

  private RollerBar roller;
  private Claw claw;
  public IntakeCargo() {
    claw = Claw.getInstance();
    roller = RollerBar.getInstance();
    requires(roller);
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    roller.setSpeed(claw.getMotorController());
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    claw.setSpeed(Robot.O_I.getIntakeSpeed());
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
