/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 * Add your docs here.
 */
public class ToggleCamera extends InstantCommand {
  /**
   * Add your docs here.
   */

  // specifies the total available camera streams and currently selected stream.
  private int camMode = 0;
    //private int maxSize = 0;

  public ToggleCamera() {
    CameraServer.getInstance().startAutomaticCapture("Top Cam", 0);
    // Listen for the raspberry to report how many streams it has.
    // NetworkTableInstance.getDefault().getTable("optics").getEntry("CamSize").addListener(camsizelistener -> {
    //   maxSize = camsizelistener.getEntry().getNumber(0).intValue();
    // }, EntryListenerFlags.kNew);
  }

  // Called once when the command executes
  @Override
  protected void initialize() {
    camMode++;
    if (camMode == 0) 
      CameraServer.getInstance().startAutomaticCapture("Top Cam", 0);
    if (camMode == 1)
      CameraServer.getInstance().startAutomaticCapture("Spear Cam", 1);
    else 
      camMode = 0;
    // Change the connected camera stream and underflow
    // if the selected stream would be greater than the number
    // of available cameras
    // camMode++;
    // if (camMode >= maxSize) {
    //   camMode = 0;
    // }

    // // Report to the raspberry pi using network tables which stream to select and
    // // display on
    // // the default mjpg stream
    // NetworkTableInstance.getDefault().getTable("optics").getEntry("CamMode").setNumber(camMode);
  }

}
