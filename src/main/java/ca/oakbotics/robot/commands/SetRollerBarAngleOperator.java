/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.subsystems.RollerArms;
import edu.wpi.first.wpilibj.command.Command;

public class SetRollerBarAngleOperator extends Command {
  public SetRollerBarAngleOperator() {
    requires(RollerArms.getInstance());
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.LOGGER.logOnce(Robot.ROLLER_ARMS);

  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double setAngle = Robot.O_I.getRollerBarAngle();

    Robot.ROLLER_ARMS.setDesiredAngle(setAngle);
    Robot.ROLLER_ARMS.rotateToAngle(setAngle);
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
