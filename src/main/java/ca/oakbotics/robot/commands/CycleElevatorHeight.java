/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.subsystems.Elevator;
import edu.wpi.first.wpilibj.command.InstantCommand;

public class CycleElevatorHeight extends InstantCommand {
  public CycleElevatorHeight() {
    super("Cycle Elevator Height");
    requires(Elevator.getInstance());
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.ELEVATOR.cyclePresetHeight(true);
  }
}
