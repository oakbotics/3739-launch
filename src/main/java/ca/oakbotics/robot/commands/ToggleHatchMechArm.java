/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.subsystems.Claw;
import edu.wpi.first.wpilibj.command.InstantCommand;

public class ToggleHatchMechArm extends InstantCommand {

  private Claw claw;
  public ToggleHatchMechArm() {
    claw = Claw.getInstance();
    requires(claw);
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    claw.toggleHatchMechArm();
  }
}
