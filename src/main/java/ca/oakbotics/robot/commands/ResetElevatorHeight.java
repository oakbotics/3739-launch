/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.subsystems.Elevator;
import edu.wpi.first.wpilibj.command.InstantCommand;

/**
 * Add your docs here.
 */
public class ResetElevatorHeight extends InstantCommand {
  /**
   * Add your docs here.
   */
  private int pos;

  public ResetElevatorHeight(int pos) {
    super("Reset Elevator Height");
    this.pos = pos;
    requires(Elevator.getInstance());
  }

  // Called once when the command executes
  @Override
  protected void initialize() {
    Robot.ELEVATOR.setPresetHeight(pos);
  }

}
