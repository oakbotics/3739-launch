/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import edu.wpi.first.wpilibj.command.Command;
import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.subsystems.Elevator;

public class SetElevatorHeightOperator extends Command {
  
  public SetElevatorHeightOperator() {
    requires(Elevator.getInstance());
    // Use requires() here to declare subsystem dependencies
    // eg. requires(chassis);
  }

  // Called just before this Command runs the first time
  @Override
  protected void initialize() {
    Robot.LOGGER.logOnce(Robot.ELEVATOR);
  }

  // Called repeatedly when this Command is scheduled to run
  @Override
  protected void execute() {
    double setpoint = Robot.O_I.getElevatorHeight();
    
   
    Robot.ELEVATOR.setHeightSetpoint(setpoint);
    Robot.ELEVATOR.setHeight(setpoint);
  }

  // Make this return true when this Command no longer needs to run execute()
  @Override
  protected boolean isFinished() {
    return false;
  }

  // Called once after isFinished returns true
  @Override
  protected void end() {
  }

  // Called when another command which requires one or more of the same
  // subsystems is scheduled to run
  @Override
  protected void interrupted() {
  }
}
