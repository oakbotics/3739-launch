/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.Constants;
import ca.oakbotics.robot.Robot;
import ca.oakbotics.robot.subsystems.Elevator;
import ca.oakbotics.robot.subsystems.RollerArms;
import edu.wpi.first.wpilibj.command.Command;
import edu.wpi.first.wpilibj.command.InstantCommand;

public class CycleElevatorAndRollerArms extends InstantCommand {
  /**
   * Add your docs here.
   */

  private boolean direction;
  private Command resetArm;

  public CycleElevatorAndRollerArms(boolean direction) {
    super("Cycle Elevator and Roller");
    requires(Elevator.getInstance());
    requires(RollerArms.getInstance());

    this.direction = direction;
    this.resetArm = new ResetArmAngle(1);
  }

  @Override
  protected void initialize() {
    Robot.ELEVATOR.cyclePresetHeight(direction);
    
    if (!Robot.ROLLER_ARMS.isArmDown() && (Robot.ELEVATOR.getHeight() <= Constants.kINTERLOCK_LOW && Robot.ELEVATOR.getHeightSetpoint() >= Constants.kINTERLOCK_LOW)) {
      resetArm.start();
    }

    else if (!Robot.ROLLER_ARMS.isArmDown() && (Robot.ELEVATOR.getHeight() >= Constants.kINTERLOCK_HIGH && Robot.ELEVATOR.getHeightSetpoint() <= Constants.kINTERLOCK_HIGH)) {
      resetArm.start();
    }

    if (Robot.ELEVATOR.getIndex() == 5) {
      Robot.INTAKE_CLAW.raiseForebar();
    }
  }
}
