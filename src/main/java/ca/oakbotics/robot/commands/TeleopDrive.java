/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.commands;

import ca.oakbotics.robot.OI;
import ca.oakbotics.robot.subsystems.DriveBase;
import edu.wpi.first.wpilibj.command.Command;

public class TeleopDrive extends Command {

  public TeleopDrive() {
    requires(DriveBase.getInstance());
  }

  @Override
  protected void initialize() {
    DriveBase.getInstance().stop();
  }

  @Override
  protected void execute() {  
    double val = OI.getInstance().getDriveSpeed();
    DriveBase.getInstance().arcadeDrive(val, OI.getInstance().getTurnSpeed());
  }

  @Override
  protected boolean isFinished() {
    // Run for the lifetime of the program
    // can be interrupted
    return false;
  }

  @Override
  protected void end() {
    DriveBase.getInstance().stop();
  }

  @Override
  protected void interrupted() {
    DriveBase.getInstance().stop();
  }
}
