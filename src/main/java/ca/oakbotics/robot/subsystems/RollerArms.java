/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.InvertType;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import ca.oakbotics.robot.Constants;
import ca.oakbotics.robot.commands.HoldArmAngle;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Add your docs here.
 */
public class RollerArms extends Subsystem implements SetpointSubsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  public final static double[] ARM_ANGLE_PRESETS = { Constants.kARM_UP_ANGLE, Constants.kARM_DOWN_ANGLE, Constants.kARM_STAGE2_INTERLOCK };
  private int currentIdx = 0;
  private double desiredArmAngle = Constants.kARM_UP_ANGLE;

  private TalonSRX armMaster;
  private VictorSPX armFollower;

  private final static int kSlotIdx = 0, kPIDLoopIdx = 0, kTimeoutMs = 10;
  private final static double kF = 0.7, kP = 0.7, kI = 0.0, kD = 0.02;

  private static RollerArms rollerArmsInstance;

  private RollerArms() {
    armMaster = new TalonSRX(Constants.CAN_ID_ROLLER_ARM_MASTER);
    armFollower = new VictorSPX(Constants.CAN_ID_ROLLER_ARM_FOLLOWER);
    armMaster.setInverted(false);
    armMaster.setSensorPhase(true); // Controller out of phase with sensor so fix that
    armMaster.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, kPIDLoopIdx, kTimeoutMs); // select
                                                                                                              // the
                                                                                                              // type
                                                                                                              // sensor
                                                                                                              // and
                                                                                                              // bind
                                                                                                              // it to
                                                                                                              // a PID
                                                                                                              // loop

    // CAN data stuff, ensures no overrun
    armMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_13_Base_PIDF0, 10, kTimeoutMs);
    armMaster.setStatusFramePeriod(StatusFrameEnhanced.Status_10_MotionMagic, 10, kTimeoutMs);

    // Setting up deadbands
    armMaster.configNominalOutputForward(0, kTimeoutMs);
    armMaster.configNominalOutputReverse(0, kTimeoutMs);
    armMaster.configPeakOutputForward(1.0, kTimeoutMs);
    armMaster.configPeakOutputReverse(-1.0, kTimeoutMs);

    // Binding the sensor "slot" to the pid loop "slot"
    armMaster.selectProfileSlot(kSlotIdx, kPIDLoopIdx);

    // PID Constants for motion magic
    armMaster.config_kF(kSlotIdx, kF, kTimeoutMs);
    armMaster.config_kP(kSlotIdx, kP, kTimeoutMs);
    armMaster.config_kI(kSlotIdx, kI, kTimeoutMs);
    armMaster.config_kD(kSlotIdx, kD, kTimeoutMs);

    // Set max velocity and acceleration, motion magic is traezoidal
    armMaster.configMotionCruiseVelocity(1000, kTimeoutMs);
    armMaster.configMotionAcceleration(800, kTimeoutMs);

    // Zero out the sensor acc, elevator is all the way down on start up
    // so throw out the previous values
    armMaster.setSelectedSensorPosition(0, kPIDLoopIdx, kTimeoutMs);

    armMaster.setSensorPhase(false);
    armFollower.follow(armMaster);
    armMaster.configAllowableClosedloopError(kSlotIdx, 50, kTimeoutMs);

    armMaster.setInverted(true);
    armFollower.setInverted(InvertType.OpposeMaster);
  }

  public static RollerArms getInstance() {
    if (rollerArmsInstance == null)
      rollerArmsInstance = new RollerArms();
    return rollerArmsInstance;
  }

  public void zeroSensor() {
    armMaster.setSelectedSensorPosition(0, kPIDLoopIdx, kTimeoutMs);
  }

  public void setPresetAngle(int pos) {
    currentIdx = pos;
    if (currentIdx >= ARM_ANGLE_PRESETS.length) {
      currentIdx = ARM_ANGLE_PRESETS.length - 1;
    }
    else if (currentIdx < 0) {
      currentIdx = 0;
    }
    setDesiredAngle(ARM_ANGLE_PRESETS[currentIdx]);
  }

  public void cyclePresetAngles(boolean isUp) {
    if (isUp) {
      currentIdx++;
      if (currentIdx == ARM_ANGLE_PRESETS.length) currentIdx = 0;
    }
    else {
      currentIdx--;
      if (currentIdx == -1) currentIdx = ARM_ANGLE_PRESETS.length - 1;
    }
    setPresetAngle(currentIdx);
  }
  public boolean isArmDown() {
    return this.getCurrentArmAngle() > 1850;
  }

  public double getDesiredAngle() {
    return this.desiredArmAngle;
  }

  public void setDesiredAngle(double angle) {
    this.desiredArmAngle = angle;
  }

  public double getCurrentArmAngle() {
    return armMaster.getSelectedSensorPosition() * Constants.kTICKS_PER_ANGLE;
  }

  public void rotateToAngle(double angle) {
    armMaster.set(ControlMode.MotionMagic, angle);
  }

  public void holdPosition() {
    armMaster.set(ControlMode.Position, desiredArmAngle);
    //armMaster.set(ControlMode.MotionMagic, desiredArmAngle);
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
    setDefaultCommand(new HoldArmAngle());
  }

  @Override
  public double getCurrentValue() {
    return getCurrentArmAngle();
  }

  @Override
  public double getSetpointValue() {
    return getDesiredAngle();
  }

  public String getSubsystemName() {
    return "Roller Arms";
  }
}
