/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.subsystems;

import java.util.ArrayList;
import java.util.List;

import com.kauailabs.navx.frc.AHRS;
import com.revrobotics.CANEncoder;
import com.revrobotics.CANSparkMax;
import com.revrobotics.CANSparkMaxLowLevel;

import ca.oakbotics.robot.commands.TeleopDrive;
import ca.oakbotics.robot.Constants;
import edu.wpi.first.wpilibj.DriverStation;
import edu.wpi.first.wpilibj.SPI;
import edu.wpi.first.wpilibj.SpeedControllerGroup;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.drive.DifferentialDrive;

/**
 * Add your docs here.
 */
public class DriveBase extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private static DriveBase driveBaseInstance;

  // Store controllers and encoders in a list for ease of instantiation
  private List<CANSparkMax> controllers = new ArrayList<>(4);
  private List<CANEncoder> encoders = new ArrayList<>(4);

  // left and right speed controller group, we will
  // never drive either of the left motors independantly
  private DifferentialDrive robotDrive;

  private AHRS navxBoard;

  private DriveBase() {

    // Instantiate encoders and controllers
    for (int i = Constants.CAN_ID_LA_DRIVE_MTR; i <= Constants.CAN_ID_RB_DRIVE_MTR; i++) {
      CANSparkMax controller = new CANSparkMax(i, CANSparkMaxLowLevel.MotorType.kBrushless);
      CANEncoder encoder = new CANEncoder(controller);

      controller.setOpenLoopRampRate(0.30);

      // Left side motors are 0, 1 or CAN id 5, 6
      // Right side motors are 2, 3 or CAN id 7, 8
      controllers.add(i - Constants.CAN_ID_LA_DRIVE_MTR, controller);
      encoders.add(i - Constants.CAN_ID_LA_DRIVE_MTR, encoder);
    }

    // Differential drive handles the left - right side inversion.
    robotDrive = new DifferentialDrive(
      new SpeedControllerGroup(controllers.get(0), controllers.get(1)),
      new SpeedControllerGroup(controllers.get(2), controllers.get(3)));

    try {
      navxBoard = new AHRS(SPI.Port.kMXP);
    } catch (RuntimeException e) {
      // navx may fail to connect, don't be vague with error messages
      DriverStation.reportError("NavX error, check for short circuits, status lights and connection", true);
    }
  }

  /**
   * Get the instance of the drive base, only 1 drive base per robot
   * @return the Drive Base object
   */
  public static DriveBase getInstance() {
    if (driveBaseInstance == null) {
      driveBaseInstance = new DriveBase();

    }

    return driveBaseInstance;
  }

  /**
   * Instantly stops output to each motor controller
   */
  public void stop() {
    for (CANSparkMax spark : controllers) {
      spark.stopMotor();
    }
  }

  /**
   * Toggles the idle mode of the motor controllers, brake mode is useful 
   * when disabling the robot to have the robot instantly "brake", drivers
   * may prefer coast mode for control.
   * 
   * @param isCoasting set to true if the robot will be in coast mode,
   * false for brake mode
   */
  public void setIdleCoast(boolean isCoasting) {
    if (isCoasting) {
      for (CANSparkMax spark : controllers) {
        spark.setIdleMode(CANSparkMax.IdleMode.kCoast);
      }
    }
    else {
      for (CANSparkMax spark : controllers) {
        spark.setIdleMode(CANSparkMax.IdleMode.kBrake);
      }
    }
  }

  /**
   * Exposes the differential drive arcade drive method to the robot program
   * for later use if we incorporate more control while arcade driving
   * 
   * @param throttle x speed of the robot
   * @param turn rotation speed of the robot
   */
  public void arcadeDrive(double throttle, double turn) {
    robotDrive.arcadeDrive(throttle, turn, true);
  }

  /**
   * Sets the automatic looping command of this subsystem
   */
  @Override
  public void initDefaultCommand() {
    // Teleop drive for automatic driver control of the robot
    setDefaultCommand(new TeleopDrive());
  }

  /**
   * Get the distance recorded by the left encoders and average the result
   * @return distance in 'UNITS' recorded by the left encoders
   */
  public double getLeftDistance() {
    // TODO: calibrate this
    return 10.75 * ((encoders.get(0).getPosition() + encoders.get(1).getPosition()) / 2) / (Math.PI * 6);
  }

  /**
   * Get the distance recorded by the right encoders and average the result
   * @return distance in 'UNITS' recorded by the right encoders
   */
  public double getRightDistance() {
    // TODO: calibrate this
    return 10.75 * ((encoders.get(2).getPosition() + encoders.get(3).getPosition()) / 2) / (Math.PI * 6);
  }

  /**
   * Helper method returns the yaw angle
   * as recorded by the NavX
   * @return Yaw angle in degrees, accumulates past 360
   * minimal drift
   */
  public double getRobotAngle() {

    return navxBoard.getAngle();
  }

  /**
   * Return the class member NavX board
   * @return the drive base' NavX
   */
  public AHRS getNavxBoard() {
    return navxBoard;
  }

}
