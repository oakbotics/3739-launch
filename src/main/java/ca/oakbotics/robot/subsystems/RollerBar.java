/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.IMotorController;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import ca.oakbotics.robot.Constants;
import ca.oakbotics.robot.commands.IntakeCargo;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Add your docs here.
 */

public class RollerBar extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private VictorSPX rollerMtr;

  private static RollerBar rollerIntakeInstance;

  private RollerBar() {
    rollerMtr = new VictorSPX(Constants.CAN_ID_ROLLER_INTAKE_MTR);

    rollerMtr.setInverted(true);
  }

  public static RollerBar getInstance() {
    if (rollerIntakeInstance == null) {
      rollerIntakeInstance = new RollerBar();
    }
    return rollerIntakeInstance;
  }

  public boolean isArmDown() {
    return false;
  }

  public void setSpeed(double speed) {
    rollerMtr.set(ControlMode.PercentOutput, speed);
  }

  public void setSpeed(IMotorController masterMtr) {
    rollerMtr.follow(masterMtr);
  }

  public IMotorController getMotorController() {
    return this.rollerMtr;
  }

  public void stop() {
    rollerMtr.set(ControlMode.Disabled, 0.0);
  }

  @Override
  public void initDefaultCommand() {
    setDefaultCommand(new IntakeCargo());
  }
}
