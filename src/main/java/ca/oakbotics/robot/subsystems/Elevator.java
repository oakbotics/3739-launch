/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.FeedbackDevice;
import com.ctre.phoenix.motorcontrol.NeutralMode;
import com.ctre.phoenix.motorcontrol.StatusFrameEnhanced;
import com.ctre.phoenix.motorcontrol.can.TalonSRX;

import ca.oakbotics.robot.Constants;
import ca.oakbotics.robot.commands.HoldElevatorPosition;
import edu.wpi.first.wpilibj.DigitalInput;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Add your docs here.
 */
public class Elevator extends Subsystem implements SetpointSubsystem {

  private static Elevator elevatorInstance;

  private final static double kF = 0.4, kP = 0.7, kI = 0.00, kD = 0.02;

  // SRX motor controllers can have multiple encoders and PID loops on different
  // "slots"
  // so we set these here, timeout is a ms delay in configuring settings on the
  // CAN
  // controllers, if 10 ms goes by and the settings didn't set, throw an error
  private final static int kSlotIdx = 0, kPIDLoopIdx = 0, kTimeoutMs = 10;

  private double setpoint = 0;
  private int currentIdx = 0;

  private TalonSRX elevatorMtr;

  private DigitalInput elevatorSensor;

  public final static double[] heights = { 0, 52.5, 66.5, 123.5, 137.5, 174.0 };


  /**
   * Add your docs here.
   */
  private Elevator() {

    super("Elevator");

    elevatorSensor = new DigitalInput(Constants.DIGITAL_IO_ELEVATOR_SENSOR);

    elevatorMtr = new TalonSRX(Constants.CAN_ID_ELEVATOR_MTR);

    elevatorMtr.setSensorPhase(false); // Controller out of phase with sensor so fix that
    elevatorMtr.configSelectedFeedbackSensor(FeedbackDevice.CTRE_MagEncoder_Absolute, kPIDLoopIdx, kTimeoutMs); // select
                                                                                                                // the
                                                                                                                // type
                                                                                                                // sensor
                                                                                                                // and
                                                                                                                // bind
                                                                                                                // it to
                                                                                                                // a PID
                                                                                                                // loop

    // CAN data stuff, ensures no overrun
    elevatorMtr.setStatusFramePeriod(StatusFrameEnhanced.Status_13_Base_PIDF0, 10, kTimeoutMs);
    elevatorMtr.setStatusFramePeriod(StatusFrameEnhanced.Status_10_MotionMagic, 10, kTimeoutMs);

    // Setting up deadbands
    elevatorMtr.configNominalOutputForward(0, kTimeoutMs);
    elevatorMtr.configNominalOutputReverse(0, kTimeoutMs);
    elevatorMtr.configPeakOutputForward(1, kTimeoutMs);
    elevatorMtr.configPeakOutputReverse(-1, kTimeoutMs);

    // Binding the sensor "slot" to the pid loop "slot"
    elevatorMtr.selectProfileSlot(kSlotIdx, kPIDLoopIdx);

    // PID Constants for motion magic
    elevatorMtr.config_kF(kSlotIdx, kF, kTimeoutMs);
    elevatorMtr.config_kP(kSlotIdx, kP, kTimeoutMs);
    elevatorMtr.config_kI(kSlotIdx, kI, kTimeoutMs);
    elevatorMtr.config_kD(kSlotIdx, kD, kTimeoutMs);

    // Set max velocity and acceleration, motion magic is traezoidal
    elevatorMtr.configMotionCruiseVelocity(1600, kTimeoutMs);
    elevatorMtr.configMotionAcceleration(1600, kTimeoutMs);

    // Zero out the sensor acc, elevator is all the way down on start up
    // so throw out the previous values
    elevatorMtr.setSelectedSensorPosition(0, kPIDLoopIdx, kTimeoutMs);
    elevatorMtr.setInverted(false);


    elevatorSensor.setName("Elevator", "limit");
    SmartDashboard.putData(elevatorSensor);
  }

  /**
   * Gets the elevator, ensures no duplicate subsystems
   * 
   * @return the Elevator subsystem
   */
  public static Elevator getInstance() {
    // Create the elevator if it hasn't been initialized
    if (elevatorInstance == null) {
      elevatorInstance = new Elevator();
    }
    return elevatorInstance;
  }

  /**
   * Drives the elevator motor in percent mode, same as PWM controllers. -1.0 is
   * maximum reverse +1.0 maximum forwards and 0.0 is no motion.
   * 
   * @param val the percent voltage to apply to the motor between -1.0 and +1.0
   */
  public void setSpeedPercent(double val) {
    if (!elevatorSensor.get() && val < 0) {
    }
    else {
      elevatorMtr.set(ControlMode.PercentOutput, val);
    }
  }

  /**
   * Uses Talon SRX built in motion control to lift the elevator to a given
   * height.
   * 
   * @param targetHeightCM The upwards displacement of the elevator in CM from the
   *                       lowest setpoint (0) of the elevator.
   */
  public void setHeight(double targetHeightCM) {

    //TODO: Get the Damn interlocks working.
      // If the elevator is below the roller and not going to move above it
    // if ((Robot.ROLLER_ARMS.isArmDown() && (targetHeightCM < Constants.kINTERLOCK_LOW) && (getHeight() < Constants.kINTERLOCK_LOW)) 
    //   // If the elevator is above the roller and not going to move below it
    //   || (Robot.ROLLER_ARMS.isArmDown() && (getHeight() > Constants.kINTERLOCK_HIGH) && (targetHeightCM > Constants.kINTERLOCK_HIGH))
    //   // If the roller is in the down position
    //   || Robot.ROLLER_ARMS.isArmDown()) {
      double targetPosition = targetHeightCM * Constants.kELEVATOR_ENCODER_UNITS_PER_CM; // calculates position in terms of sensor units

      elevatorMtr.set(ControlMode.MotionMagic, targetPosition);
    // }
  }

  public void cyclePresetHeight(boolean isUp) {
    if (isUp) {
      currentIdx++;
      if (currentIdx == heights.length) currentIdx = 0;
    }
    else {
      currentIdx--;
      if (currentIdx == -1) currentIdx = heights.length - 1;
    }
    setPresetHeight(currentIdx);
  }

  public int getIndex() {
    return currentIdx;
  }

  public double getHeight() {
    return elevatorMtr.getSelectedSensorPosition() / Constants.kELEVATOR_ENCODER_UNITS_PER_CM;
  }

  public void setHeightSetpoint(double heightSetpoint) {
    this.setpoint = heightSetpoint;
  }

  public double getHeightSetpoint() {
    return setpoint;
  }

  public void setPresetHeight(int pos) {
    currentIdx = pos;
    if (currentIdx >= heights.length) {
      currentIdx = 0;
    }
    else if (currentIdx < 0) {
      currentIdx = heights.length - 1;
    }
    setHeightSetpoint(heights[currentIdx]);
  }

  public void setBrakeMode(boolean val) {
    if (val) elevatorMtr.setNeutralMode(NeutralMode.Brake);
    else elevatorMtr.setNeutralMode(NeutralMode.Coast);

  }
  public void stop() {
    elevatorMtr.set(ControlMode.Disabled, 0.0);
  }

  public void zeroElevatorSensor() {
    elevatorMtr.setSelectedSensorPosition(0, kPIDLoopIdx, kTimeoutMs);
  }

  @Override
  public void initDefaultCommand() {

    // setDefaultCommand(new SetElevatorHeightOperator());
    setDefaultCommand(new HoldElevatorPosition());

    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }

  @Override
  public double getCurrentValue() {
    return getHeight();
  }

  @Override
  public double getSetpointValue() {
    return getHeightSetpoint();
  }

  public String getSubsystemName() {
    return "Elevator";
  }
}
