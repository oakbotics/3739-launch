/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.subsystems;

import ca.oakbotics.robot.Constants;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;

/**
 * Add your docs here.
 */
public class Climber extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.
  private DoubleSolenoid climbPiston;

  private static Climber climberInstance;

  private Climber() {
    climbPiston = new DoubleSolenoid(Constants.PCM_CHANNEL_CLIMB_POLE_UP, Constants.PCM_CHANNEL_CLIMB_POLE_DOWN);

  }

  public static Climber getInstance() {
    if (climberInstance == null) climberInstance = new Climber();
    return climberInstance;
  }

  public void extendPole() {
    climbPiston.set(Value.kForward);
  }

  public void retractPole() {
    climbPiston.set(Value.kReverse);
  }

  public void togglePole() {
    if (climbPiston.get() != Value.kForward) {
      System.out.println("Pole up");
      climbPiston.set(Value.kForward);
    }
    else {
      System.out.println("Pole down");
      climbPiston.set(Value.kReverse);
    }
  }

  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }
}
