/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot.subsystems;

import com.ctre.phoenix.motorcontrol.ControlMode;
import com.ctre.phoenix.motorcontrol.IMotorController;
import com.ctre.phoenix.motorcontrol.can.VictorSPX;

import ca.oakbotics.robot.Constants;
import edu.wpi.first.wpilibj.DoubleSolenoid;
import edu.wpi.first.wpilibj.DoubleSolenoid.Value;
import edu.wpi.first.wpilibj.command.Subsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * Add your docs here.
 */
public class Claw extends Subsystem {
  // Put methods for controlling this subsystem
  // here. Call these from Commands.

  private DoubleSolenoid forebarDS;
  private DoubleSolenoid hatchMechArmDS;
  private VictorSPX intakeMtr;
  private DoubleSolenoid hatchMechGrabberDS;
  //TT
  private static Claw intakeClawInstance;

  private Claw() {
    hatchMechArmDS = new DoubleSolenoid(Constants.CAN_ID_PCM, Constants.PCM_CHANNEL_HATCH_MECH_ARM_OUT, Constants.PCM_CHANNEL_HATCH_MECH_ARM_IN);
    forebarDS = new DoubleSolenoid(Constants.CAN_ID_PCM, Constants.PCM_CHANNEL_FOUR_BAR_UP, Constants.PCM_CHANNEL_FOUR_BAR_DOWN);
    intakeMtr = new VictorSPX(Constants.CAN_ID_CLAW_INTAKE_MTR);
    hatchMechGrabberDS = new DoubleSolenoid(Constants.CAN_ID_PCM, Constants.PCM_CHANNEL_HATCH_MECH_GRABBER_OUT, Constants.PCM_CHANNEL_HATCH_MECH_GRABBER_IN);
    intakeMtr.setInverted(true);

    forebarDS.setName("Claw", "Forebar");
    hatchMechArmDS.setName("Claw", "Hatch Arm");
    hatchMechGrabberDS.setName("Claw", "Hatch Grabber");
    SmartDashboard.putData(forebarDS);
    SmartDashboard.putData(hatchMechArmDS);
    SmartDashboard.putData(hatchMechGrabberDS);
  }

  public static Claw getInstance() {
    if (intakeClawInstance == null) {
      intakeClawInstance = new Claw();
    }
    return intakeClawInstance;
  }
  
  public void toggleHatchMechGrabber(){
    if (hatchMechGrabberDS.get() == Value.kForward) retractHatchMechGrabber();
    else extendHatchMechGrabber();
  }

  public void extendHatchMechGrabber(){
    hatchMechGrabberDS.set(Value.kForward);
  }

  public void retractHatchMechGrabber(){
    hatchMechGrabberDS.set(Value.kReverse);
  }
  //TT
  public void extendHatchMechArm() {
    hatchMechArmDS.set(Value.kForward);
  }

  public void retractHatchMechArm() {
    hatchMechArmDS.set(Value.kReverse);
  }
  public void toggleHatchMechArm(){
    if (hatchMechArmDS.get() == Value.kForward) retractHatchMechArm();
    else extendHatchMechArm();
  }
  public void raiseForebar() {
    forebarDS.set(Value.kForward);
  }

  public void lowerForebar() {
    forebarDS.set(Value.kReverse);
  }

  public void toggleForebar(){
    if(forebarDS.get() == Value.kForward) lowerForebar();
    else raiseForebar();
  }
  
  public void setSpeed(double speed) {
    intakeMtr.set(ControlMode.PercentOutput, speed);
  }

  public void setSpeed(IMotorController motorController) {
    intakeMtr.follow(motorController);
  }

  public IMotorController getMotorController () {
    return intakeMtr;
  }

  public void stop() {
    intakeMtr.set(ControlMode.Disabled, 0.0);
    forebarDS.set(Value.kOff);
    hatchMechArmDS.set(Value.kOff);
    hatchMechGrabberDS.set(Value.kOff);
  }


  @Override
  public void initDefaultCommand() {
    // Set the default command for a subsystem here.
    // setDefaultCommand(new MySpecialCommand());
  }
}
