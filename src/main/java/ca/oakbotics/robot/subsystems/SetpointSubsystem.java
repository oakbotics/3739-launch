package ca.oakbotics.robot.subsystems;

public interface SetpointSubsystem {
    public double getCurrentValue();

    public double getSetpointValue();

    public String getSubsystemName();
}