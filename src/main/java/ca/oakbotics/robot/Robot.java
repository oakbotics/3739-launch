/*----------------------------------------------------------------------------*/
/* Copyright (c) 2017-2018 FIRST. All Rights Reserved.                        */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ca.oakbotics.robot.commands.ZeroElevatorSensor;
import ca.oakbotics.robot.commands.ZeroRollerArmsSensor;
import ca.oakbotics.robot.subsystems.Claw;
import ca.oakbotics.robot.subsystems.Climber;
import ca.oakbotics.robot.subsystems.DriveBase;
import ca.oakbotics.robot.subsystems.Elevator;
import ca.oakbotics.robot.subsystems.RollerArms;
import ca.oakbotics.robot.subsystems.RollerBar;
import edu.wpi.first.cameraserver.CameraServer;
import edu.wpi.first.networktables.NetworkTableInstance;
import edu.wpi.first.wpilibj.Compressor;
import edu.wpi.first.wpilibj.PowerDistributionPanel;
import edu.wpi.first.wpilibj.RobotController;
import edu.wpi.first.wpilibj.Spark;
import edu.wpi.first.wpilibj.TimedRobot;
import edu.wpi.first.wpilibj.Timer;
import edu.wpi.first.wpilibj.command.Scheduler;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

/**
 * The VM is configured to automatically run this class, and to call the
 * functions corresponding to each mode, as described in the TimedRobot
 * documentation. If you change the name of this class or the package after
 * creating this project, you must also update the build.gradle file in the
 * project.
 */
public class Robot extends TimedRobot {
  public final static NetworkTableInstance NTINST = NetworkTableInstance.getDefault();
  public final static PowerDistributionPanel PDP = new PowerDistributionPanel(Constants.CAN_ID_PDP);

  // Subsystems
  public final static OI O_I = OI.getInstance();
  public final static DriveBase DRIVE_BASE = DriveBase.getInstance();
  public final static Elevator ELEVATOR = Elevator.getInstance();
  public final static Claw INTAKE_CLAW = Claw.getInstance();
  public final static RollerBar INTAKE_ROLLER = RollerBar.getInstance();
  public final static RollerArms ROLLER_ARMS = RollerArms.getInstance();
  public final static Climber CLIMBER = Climber.getInstance();
  public final static Logger LOGGER = Logger.getInstance();

  private final static Spark LED_DRIVER = new Spark(9);
  public final static Compressor COMPRESSOR = new Compressor(Constants.CAN_ID_PCM);

  private static double tPrevious = 0, tCurrent = 0;



  /**
   * This function is run when the robot is first started up and should be
   * used for any initialization code.
   */
  @Override
  public void robotInit() {
    LOGGER.add(ELEVATOR);
    LOGGER.add(ROLLER_ARMS);
    ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    scheduler.scheduleAtFixedRate(LOGGER, 0, 500, TimeUnit.MILLISECONDS);
    CameraServer.getInstance().startAutomaticCapture();

    ELEVATOR.setBrakeMode(true);
    tCurrent = Timer.getFPGATimestamp();

    SmartDashboard.putData(new ZeroElevatorSensor());
    SmartDashboard.putData(new ZeroRollerArmsSensor());
  }

  /**
   * This function is called every robot packet, no matter the mode. Use
   * this for items like diagnostics that you want ran during disabled,
   * autonomous, teleoperated and test.
   *
   * <p>This runs after the mode specific periodic functions, but before
   * LiveWindow and SmartDashboard integrated updating.
   */
  @Override
  public void robotPeriodic() {
    tPrevious = tCurrent;
    tCurrent = Timer.getFPGATimestamp();
  }

  public static double getLastLoopTime() {
    return tCurrent - tPrevious;
  }

  @Override
  public void disabledInit() {
    DRIVE_BASE.setIdleCoast(false);
  }

  @Override
  public void disabledPeriodic() {
    LED_DRIVER.set(0.65);
    if (RobotController.getUserButton()) {
      ROLLER_ARMS.zeroSensor();
      ELEVATOR.setPresetHeight(0);
    }
  }

  /**
   * This autonomous (along with the chooser code above) shows how to select
   * between different autonomous modes using the dashboard. The sendable
   * chooser code works with the Java SmartDashboard. If you prefer the
   * LabVIEW Dashboard, remove all of the chooser code and uncomment the
   * getString line to get the auto name from the text box below the Gyro
   *
   * <p>You can add additional auto modes by adding additional comparisons to
   * the switch structure below with additional strings. If using the
   * SendableChooser make sure to add them to the chooser code above as well.
   */
  @Override
  public void autonomousInit() {
    DRIVE_BASE.setIdleCoast(true);

  }

  /**
   * This function is called periodically during autonomous.
   */
  @Override
  public void autonomousPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void teleopInit() {
    DRIVE_BASE.setIdleCoast(true);
  }

  /**
   * This function is called periodically during operator control.
   */
  @Override
  public void teleopPeriodic() {
    Scheduler.getInstance().run();
  }

  @Override
  public void testInit() {
    DRIVE_BASE.setIdleCoast(true);
  }
  /**
   * This function is called periodically during test mode.
   */
  @Override
  public void testPeriodic() {
    LED_DRIVER.set(0.65);
  }
}