package ca.oakbotics.robot;

import java.util.ArrayList;
import java.util.List;

import ca.oakbotics.robot.subsystems.SetpointSubsystem;
import edu.wpi.first.wpilibj.smartdashboard.SmartDashboard;

public class Logger implements Runnable {
    private final static int k_PRINT_PERIOD_S = 250;
    private final static int k_START_CAP = 2;
    private List<SetpointSubsystem> trackedSubsystems;

    private static Logger loggerInstance = null;

    private Logger() {
        System.out.println("Starting subsystem logging");
        trackedSubsystems = new ArrayList<>(k_START_CAP);
    }

    public static Logger getInstance() {
        if (loggerInstance == null) {
            loggerInstance = new Logger();
        }
        return loggerInstance;
    }

    public void add(SetpointSubsystem sub) {
        trackedSubsystems.add(sub);
    }

    public void logOnce() {
        for (SetpointSubsystem s : trackedSubsystems) {
            this.logOnce(s);
        }
    }

    public void logOnce(SetpointSubsystem sub) {
        SmartDashboard.putNumber(sub.getSubsystemName() + " Current", sub.getCurrentValue());
        SmartDashboard.putNumber(sub.getSubsystemName() + " Setpoint", sub.getSetpointValue());
        System.out.println(sub.getSubsystemName() + " Current: " + sub.getCurrentValue());
        System.out.println(sub.getSubsystemName() + " Setpoint: " + sub.getSetpointValue());
    }

    @Override
    public void run() {
        this.logOnce();
       
    }
}