/*----------------------------------------------------------------------------*/
/* Copyright (c) 2018 FIRST. All Rights Reserved.                             */
/* Open Source Software - may be modified and shared by FRC teams. The code   */
/* must be accompanied by the FIRST BSD license file in the root directory of */
/* the project.                                                               */
/*----------------------------------------------------------------------------*/

package ca.oakbotics.robot;

/**
 * Define robot wide constants such as PWM / CAN outputs
 * Place any constant that would conflict with another assignment, such as ports and ids
 * or that will be useful in more than just one class.
 * 
 * For example PID scaling constants can be placed as members within their container class.
 */
public class Constants {

    // Define CAN ID's for every CAN device
    public final static int CAN_ID_PDP = 1;
    public final static int CAN_ID_PCM = 0;
    public final static int CAN_ID_LA_DRIVE_MTR = 5;
    public final static int CAN_ID_LB_DRIVE_MTR = 6;
    public final static int CAN_ID_RA_DRIVE_MTR = 7;
    public final static int CAN_ID_RB_DRIVE_MTR = 8;
    public final static int CAN_ID_ELEVATOR_MTR = 9;
    public final static int CAN_ID_CLAW_INTAKE_MTR = 10;
    public final static int CAN_ID_ROLLER_INTAKE_MTR = 11;
    public final static int CAN_ID_ROLLER_ARM_MASTER = 12;
    public final static int CAN_ID_ROLLER_ARM_FOLLOWER = 13;

    // Declare PCM Channels for each Solenoid
    public final static int PCM_CHANNEL_CLIMB_POLE_UP = 0;
    public final static int PCM_CHANNEL_CLIMB_POLE_DOWN = 7;
    public final static int PCM_CHANNEL_FOUR_BAR_UP = 1;
    public final static int PCM_CHANNEL_FOUR_BAR_DOWN = 6;
    public final static int PCM_CHANNEL_HATCH_MECH_ARM_OUT = 2;
    public final static int PCM_CHANNEL_HATCH_MECH_ARM_IN = 5;

    public final static int PCM_CHANNEL_HATCH_MECH_GRABBER_OUT = 3;
    public final static int PCM_CHANNEL_HATCH_MECH_GRABBER_IN = 4;

    // Declare misc. sensors on digital and analog lines
    public final static int DIGITAL_IO_ELEVATOR_SENSOR = 9;

    public final static double kINTERLOCK_LOW = 14.5;
    public final static double kINTERLOCK_HIGH = 79.5;

    public final static double kELEVATOR_MAX_HEIGHT = 175.4;


    public final static double kARM_UP_ANGLE = 1180;
    public final static double kARM_DOWN_ANGLE = 3030;
    public final static double kARM_STAGE2_INTERLOCK = 490;
    public final static double kARM_MAX_ANGLE = 4500;
    public final static double kTICKS_PER_ANGLE = 1.0;

    public final static double kELEVATOR_ENCODER_UNITS_PER_CM = 204.222; // nice

    
}
