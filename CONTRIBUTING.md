**Branches**
`master` will remain protected and should be merged to regularly. Merges to master must be tested on the current production bot. Each new subsystem or feature set will be branched from and only from `master`.
Branches will be rebased on to `master` only when necessary.

**Commit**
Commits must properly identify who contributed the changes and must have a descriptive title. Try to include details if possible on what the change accomplishes. 
Commit regularly when adding or modifying files, it makes your block diagram look prettier.